local S = core.get_translator("sumo")

achvmt_lib.register_mod("sumo", {
    name = S("Sumo"),
    icon = "magiccompass_sumo.png",
})




function sumo.award(p_name, ach_name)
    if not achvmt_lib.has_achievement(p_name, ach_name) then
        achvmt_lib.award(p_name, ach_name)
    end
end

-- sumo achievements



-- Can't touch this: win without ever dying
achvmt_lib.register_achievement("sumo:cant_touch_this", {
    title = S("Can't touch this"),
    description = S("Win without ever dying"),
    image = "sumo_ach_cant_touch_this.png",
    tier = "Bronze",
})

-- Reversi: switch places with someone who is then eliminated

achvmt_lib.register_achievement("sumo:reversi", {
    title = S("Reversi"),
    description = S("Eliminate a player via a switch"),
    additional_info = S("Get an elimination from the game\nusing the switch-places action"),
    image = "sumo_ach_reversi.png",
    tier = "Bronze",
})

-- Sole Executioner: Be the only one to eliminate all other players in a 3+ player game

achvmt_lib.register_achievement("sumo:sole_executioner", {
    title = S("Sole Executioner"),
    description = S("Singlehandedly unalive everyone"),
    additional_info = S("3+ players required, must personally remove all lives"),
    image = "sumo_ach_sole_executioner.png",
    tier = "Silver",
})

-- Grand Executioner: Be the only one to eliminate all other players in a 3+ player game while not being eliminated yourself

achvmt_lib.register_achievement("sumo:grand_executioner", {
    title = S("Grand Executioner"),
    description = S("Singlehandedly unalive everyone, with full ❤"), -- heart emoji:
    additional_info = S("3+ players required, must personally remove all lives"),
    image = "sumo_ach_grand_executioner.png",
    tier = "Gold",
})