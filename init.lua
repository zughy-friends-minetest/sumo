local S = core.get_translator("sumo")
sumo = {}
sumo.invincible = {}
dofile(core.get_modpath("sumo") .. "/settings.lua")

-- local value settings
local player_speed = sumo.player_speed -- when in the minigame, though players will be a little faster when running
local player_jump = sumo.player_jump -- when in the minigame, though players can jump a little higher when running

local register_achievements = core.settings:get("sumo_register_acheivements") or false
sumo.debug = core.settings:get_bool("sumo_debug") or false


arena_lib.register_minigame("sumo", {
    name = S("Sumo"),
    icon = "magiccompass_sumo.png",
    prefix = "[Sumo] ",
    show_minimap = false,
    time_mode = "decremental",
    join_while_in_progress = false,
    spectate_mode = "all",
    keep_inventory = false,
    in_game_physics = {
      speed = player_speed,
      jump = player_jump,
      sneak = false,
      sneak_glitch = false,
      gravity = 1,
    },
    
    load_time = 4,
    hotbar = {
      slots = 1,
      background_image = "sumo_gui_hotbar.png"
    },

    sounds = {
      eliminate = false
    },

    disabled_damage_types = {"punch","fall","set_hp"},
    properties = {
      jail_pos = {x = 0, y = 0, z = 0},
      lives = 3,
    },
    temp_properties = {
      speed = player_speed,
      jump = player_jump,
    },

    player_properties = {
      run_start_time = 0.0,
      running = false,
      run_timeout = 3, --players can't run for 3 sec after match start
      lives = 3,
      last_punched = nil,
      kill_log = {},
    },
})


dofile(core.get_modpath("sumo") .. "/items.lua")
dofile(core.get_modpath("sumo") .. "/minigame_manager.lua")
dofile(core.get_modpath("sumo") .. "/nodes.lua")
dofile(core.get_modpath("sumo") .. "/privs.lua")

function sumo.award(p_name, ach_name) return end
if core.get_modpath("achievements_lib") and register_achievements then
  dofile(core.get_modpath("sumo") .. "/achievements.lua")
end

sumo.get_s_time = function()
  return core.get_us_time() / 1000000
end