local S = core.get_translator("sumo")

core.register_privilege("sumo_admin", {
    description = S("With this you can use /sumo create, edit")
})
